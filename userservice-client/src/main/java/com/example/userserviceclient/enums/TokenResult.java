package com.example.userserviceclient.enums;

public enum TokenResult {
    VALID, INVALID, EXPIRED
}
