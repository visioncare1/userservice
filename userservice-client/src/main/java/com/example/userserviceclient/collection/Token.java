package com.example.userserviceclient.collection;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.util.Calendar;
import java.util.Date;

@Data
@NoArgsConstructor
public class Token {

    // Expiration time in minutes
    private static final int EXPIRATION_TIME = 10;
    @Id
    private String id;
    private String tokenString;
    private Date expirationTime;
    private String userId;

    public Token(String userId, String tokenString) {
        this.userId = userId;
        this.tokenString = tokenString;
        this.expirationTime = calculateExpirationDate();
    }

    public void refreshExpirationTime() {
        this.expirationTime = calculateExpirationDate();
    }

    private Date calculateExpirationDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(new Date().getTime());
        calendar.add(Calendar.MINUTE, EXPIRATION_TIME);
        return new Date(calendar.getTime().getTime());
    }
}
