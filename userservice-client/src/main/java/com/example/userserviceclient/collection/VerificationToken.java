package com.example.userserviceclient.collection;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * TODO:
 *      This can be saved differently i.e. instead of saving it in
 *      mongodb database, we can save it in redis for fast retrieval??
 */
@Document(collection = "verification_token")
@Data
@NoArgsConstructor
public class VerificationToken extends Token {

    public VerificationToken(String userId, String token) {
        super(userId, token);
    }
}
