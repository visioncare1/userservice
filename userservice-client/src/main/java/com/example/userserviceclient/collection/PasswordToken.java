package com.example.userserviceclient.collection;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "verification_token")
@Data
@NoArgsConstructor
public class PasswordToken extends Token {

    public PasswordToken(String userId, String token) {
        super(userId, token);
    }
}
