package com.example.userserviceclient.collection;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
class Period {
    private Long from;
    private Long to;
}

@Data
@NoArgsConstructor
@AllArgsConstructor
class TimeCustom {
    private Integer hour;
    private Integer minute;
}

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Goal {
    private String goalId;
    private Period period;
    private String name;
    private String summary;
    private TimeCustom timeDedicated;
    private List<String> notifyInterval;
}
