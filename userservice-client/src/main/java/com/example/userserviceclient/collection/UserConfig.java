package com.example.userserviceclient.collection;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserConfig {
    private Integer restInterval;
    private Integer restIntervalHeavy;
    private Integer restIntervalLight;
    private Integer restIntervalMixed;

    private Integer workInterval;
    private Integer workIntervalHeavy;
    private Integer workIntervalLight;
    private Integer workIntervalMixed;
}
