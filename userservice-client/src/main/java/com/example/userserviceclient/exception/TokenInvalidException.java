package com.example.userserviceclient.exception;

public class TokenInvalidException extends Exception {
    public TokenInvalidException(String message) {
        super(message);
    }
}
