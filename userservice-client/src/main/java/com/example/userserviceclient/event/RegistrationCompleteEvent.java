package com.example.userserviceclient.event;

import com.example.userserviceclient.collection.User;

public class RegistrationCompleteEvent extends UserEvent {
    public RegistrationCompleteEvent(User user, String applicationUrl) {
        super(user, applicationUrl);
    }
}
