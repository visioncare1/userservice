package com.example.userserviceclient.event;

import com.example.userserviceclient.collection.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class UserEvent extends ApplicationEvent {

    private transient User user;

    //TODO: Create an ApplicationUrl class??
    private String applicationUrl;

    public UserEvent(User user, String applicationUrl) {
        super(user);
        this.user = user;
        this.applicationUrl = applicationUrl;
    }
}
