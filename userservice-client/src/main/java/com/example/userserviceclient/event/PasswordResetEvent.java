package com.example.userserviceclient.event;

import com.example.userserviceclient.collection.User;

public class PasswordResetEvent extends UserEvent {
    public PasswordResetEvent(User user, String applicationUrl) {
        super(user, applicationUrl);
    }
}
