package com.example.userserviceclient.event.listener;

import com.example.userserviceclient.collection.User;
import com.example.userserviceclient.event.RegistrationCompleteEvent;
import com.example.userserviceclient.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@Slf4j
public class RegistrationCompleteEventListener implements ApplicationListener<RegistrationCompleteEvent> {

    @Autowired
    private UserService userService;

    @Override
    public void onApplicationEvent(RegistrationCompleteEvent event) {
        // Create a verification token for the user with the link
        User user = event.getUser();
        String token = UUID.randomUUID().toString();

        // TODO: refactor -> database is pinged twice
        // TODO: possible solution would be to embed the verification token in user collection
        userService.saveVerificationToken(user, token);
        String url = event.getApplicationUrl()
                        + "/verifyRegistration?token="
                        + token;


        System.out.printf("Click the link to verify your account: %s\n", url);


    }
}
