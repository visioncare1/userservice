package com.example.userserviceclient.event.listener;

import com.example.userserviceclient.collection.User;
import com.example.userserviceclient.event.PasswordResetEvent;
import com.example.userserviceclient.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class PasswordResetEventListener implements ApplicationListener<PasswordResetEvent> {

    @Autowired
    UserService userService;

    @Override
    public void onApplicationEvent(PasswordResetEvent event) {
        // Create a verification token for the user with the link
        User user = event.getUser();
        String token = UUID.randomUUID().toString();

        // TODO: refactor -> database is pinged twice
        // TODO: possible solution would be to embed the verification token in user collection
        userService.savePasswordResetToken(user, token);
        String url = event.getApplicationUrl()
                + "savePassword?token="
                + token;


        System.out.printf("Click the link to verify your account: %s%n", url);
    }
}
