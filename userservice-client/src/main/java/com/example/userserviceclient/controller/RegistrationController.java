package com.example.userserviceclient.controller;

import com.example.userserviceclient.collection.PasswordToken;
import com.example.userserviceclient.collection.User;
import com.example.userserviceclient.collection.VerificationToken;
import com.example.userserviceclient.enums.TokenResult;
import com.example.userserviceclient.event.PasswordResetEvent;
import com.example.userserviceclient.event.RegistrationCompleteEvent;
import com.example.userserviceclient.exception.TokenInvalidException;
import com.example.userserviceclient.exception.UserNotFoundException;
import com.example.userserviceclient.model.PasswordModel;
import com.example.userserviceclient.model.UserModel;
import com.example.userserviceclient.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
public class RegistrationController {

    private final Logger logger =
            LoggerFactory.getLogger(RegistrationController.class);

    @Autowired
    UserService userService;

    @Autowired
    private ApplicationEventPublisher publisher;

    @GetMapping("/api/test")
    public String testOath2Api() {
        return "nothing";
    }

    @PostMapping("/check")
    public String checkAndGetUser(@RequestBody UserModel userModel) {
        try {
            User user = userService.getUserByEmail(userModel.getEmail());
            if(userService.checkIfValidPassword(user, userModel.getPassword())) {
                logger.info("Verified successfully user with id: {}", user.getId());
                return user.getId();
            } else {
                logger.info("Bad credentials for user with id: {}", user.getId());
                return "BAD_CREDENTIALS";
            }
        } catch(UserNotFoundException e) {
            logger.info("User not found with email: {}", userModel.getEmail());
            return "NOT_FOUND";
        }
    }

    @PostMapping("/register")
    public String registerUser(@RequestBody UserModel userModel, final HttpServletRequest request) {

        User user = userService.registerUser(userModel);
        publisher.publishEvent(new RegistrationCompleteEvent(
                user,
                getApplicationUrl(request)
        ));

        return user.getId().trim();
    }

    @GetMapping("/verifyRegistration")
    public String verifyRegistration(@RequestParam("token") String token) {
        TokenResult result = userService.validateVerificationToken(token);
        switch(result) {
            case INVALID -> {
                return "Registration token is invalid";
            }
            case EXPIRED -> {
                return "Registration token is expired";
            }
        }

        return "Registration is verified successfully";
    }

    @GetMapping("/resendVerifyToken")
    public String resendVerifyToken(@RequestParam("token") String oldToken, HttpServletRequest request) throws TokenInvalidException {
        VerificationToken verificationToken = userService.generateNewVerificationToken(oldToken);
        String url = getApplicationUrl(request)
                + "verifyRegistration?token="
                + verificationToken.getTokenString();

        // send email
        System.out.println(url);

        return "Verification link is sent";
    }

    @PostMapping("/resetPassword")
    public String resetPassword(@RequestBody PasswordModel passwordModel, HttpServletRequest request) {
        try {
            User user = userService.getUserByEmail(passwordModel.getEmail());
            publisher.publishEvent(new PasswordResetEvent(
                    user,
                    getApplicationUrl(request)
            ));
            return "Reset Password link is sent";
        } catch (UserNotFoundException e) {
            return e.getMessage();
        }
    }

    @PostMapping("/savePassword")
    public String savePassword(@RequestParam("token") String token,
                               @RequestBody PasswordModel passwordModel) throws TokenInvalidException, UserNotFoundException {
        TokenResult tokenResult = userService.validatePasswordResetToken(token);
        switch(tokenResult) {
            case INVALID -> {
                return "Password token token is invalid";
            }
            case EXPIRED -> {
                return "Password token token is expired";
            }
        }

        PasswordToken passwordToken = userService.getPasswordTokenByToken(token);
        User user = userService.getUserById(passwordToken.getUserId());
        userService.savePassword(user, passwordModel.getNewPassword());

        return "Password is reset successfully";
    }

    @PostMapping("/changePassword")
    public String changePassword(@RequestBody PasswordModel passwordModel) throws UserNotFoundException {
        User user = userService.getUserByEmail(passwordModel.getEmail());
        if(!userService.checkIfValidPassword(user, passwordModel.getOldPassword())) {
            return "Old password is incorrect";
        }

        userService.savePassword(user, passwordModel.getNewPassword());
        return "Password is changed successfully";
    }

    public String getApplicationUrl(HttpServletRequest request) {
        return "http://"
                + request.getServerName()
                + ":" +
                request.getServerPort() +
                request.getContextPath();
    }
}
