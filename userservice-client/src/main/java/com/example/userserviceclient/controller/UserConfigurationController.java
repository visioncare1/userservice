package com.example.userserviceclient.controller;

import com.example.userserviceclient.collection.UserConfig;
import com.example.userserviceclient.exception.UserNotFoundException;
import com.example.userserviceclient.model.ConfigModel;
import com.example.userserviceclient.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserConfigurationController {

    @Autowired
    UserService userService;

    @PutMapping("/config/{id}")
    public String saveConfiguration(@PathVariable String id, @RequestBody ConfigModel configModel) throws UserNotFoundException {
        userService.saveUserConfig(id, configModel);

        return "OK";
    }

    @GetMapping("/config/{id}")
    public UserConfig getConfiguration(@PathVariable String id) throws UserNotFoundException {
        return userService.getUserConfig(id);
    }
}
