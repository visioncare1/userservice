package com.example.userserviceclient.controller;

import com.example.userserviceclient.collection.Goal;
import com.example.userserviceclient.exception.UserNotFoundException;
import com.example.userserviceclient.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GoalsController {

    private final Logger logger =
            LoggerFactory.getLogger(GoalsController.class);

    @Autowired
    UserService userService;

    @PostMapping("/goals/{userId}")
    String addUserGoal(@PathVariable String userId, @RequestBody Goal goal) throws UserNotFoundException {
        logger.info("Goal POST endpoint was pinged for user with id: {}", userId);

        this.userService.saveUserGoal(userId, goal);

        return "OK";
    }

    @GetMapping("/goals/{userId}")
    List<Goal> getUserGoals(@PathVariable String userId) throws UserNotFoundException {
        logger.info("Goal GET endpoint was pinged for user with id: {}", userId);

        return this.userService.getUserGoals(userId);
    }

    @DeleteMapping("/goals/{userId}/{goalId}")
    String removeUserGoal(@PathVariable String userId, @PathVariable String goalId) throws UserNotFoundException {
        logger.info("Goal DELETE endpoint was pinged for user with id: {}", userId);

        this.userService.removeUserGoal(userId, goalId);

        return "OK";
    }
}
