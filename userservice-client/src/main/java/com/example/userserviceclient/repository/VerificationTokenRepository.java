package com.example.userserviceclient.repository;

import com.example.userserviceclient.collection.VerificationToken;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VerificationTokenRepository extends MongoRepository<VerificationToken, Long> {

    Optional<VerificationToken> findByTokenString(String token);
}
