package com.example.userserviceclient.repository;

import com.example.userserviceclient.collection.PasswordToken;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface PasswordTokenRepository extends MongoRepository<PasswordToken, String> {
    Optional<PasswordToken> findByTokenString(String token);
}
