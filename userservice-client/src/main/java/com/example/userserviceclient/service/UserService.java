package com.example.userserviceclient.service;

import com.example.userserviceclient.collection.*;
import com.example.userserviceclient.enums.TokenResult;
import com.example.userserviceclient.exception.TokenInvalidException;
import com.example.userserviceclient.exception.UserNotFoundException;
import com.example.userserviceclient.model.ConfigModel;
import com.example.userserviceclient.model.UserModel;

import java.util.List;

public interface UserService {


    User registerUser(UserModel userModel);

    void saveVerificationToken(User user, String token);

    TokenResult validateVerificationToken(String token);

    VerificationToken generateNewVerificationToken(String oldToken) throws TokenInvalidException;

    User getUserByEmail(String email) throws UserNotFoundException;

    void savePasswordResetToken(User user, String token);

    TokenResult validatePasswordResetToken(String token);

    PasswordToken getPasswordTokenByToken(String token) throws TokenInvalidException;

    User getUserById(String userId) throws UserNotFoundException;

    void savePassword(User user, String newPassword);

    boolean checkIfValidPassword(User user, String password);

    void saveUserConfig(String userId, ConfigModel config) throws UserNotFoundException;
    UserConfig getUserConfig(String userId) throws UserNotFoundException;

    void saveUserGoal(String id, Goal goal) throws UserNotFoundException;

    List<Goal> getUserGoals(String id) throws UserNotFoundException;

    void removeUserGoal(String userId, String goalId) throws UserNotFoundException;
}
