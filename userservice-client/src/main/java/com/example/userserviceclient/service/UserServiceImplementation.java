package com.example.userserviceclient.service;

import com.example.userserviceclient.collection.*;
import com.example.userserviceclient.enums.TokenResult;
import com.example.userserviceclient.exception.TokenInvalidException;
import com.example.userserviceclient.exception.UserNotFoundException;
import com.example.userserviceclient.model.ConfigModel;
import com.example.userserviceclient.model.UserModel;
import com.example.userserviceclient.repository.PasswordTokenRepository;
import com.example.userserviceclient.repository.UserRepository;
import com.example.userserviceclient.repository.VerificationTokenRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceImplementation implements UserService {

    private final Logger logger =
            LoggerFactory.getLogger(UserServiceImplementation.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private VerificationTokenRepository verificationTokenRepository;

    @Autowired
    private PasswordTokenRepository passwordTokenRepository;

    @Override
    public User registerUser(UserModel userModel) {
        var userFromDb= userRepository.findUserByEmail(userModel.getEmail());

        if(userFromDb.isPresent()) {
            throw new IllegalStateException("This user with this email already exists!");
        }

        UserConfig userConfig = UserConfig.builder()
                .workInterval(60)
                .restInterval(60)
                .workIntervalHeavy(10)
                .workIntervalLight(0)
                .workIntervalMixed(5)
                .restIntervalHeavy(10)
                .restIntervalLight(0)
                .restIntervalMixed(5)
                .build();

        User user = User.builder()
                .email(userModel.getEmail())
                .role("USER")
                .userConfig(userConfig)
                .password(passwordEncoder.encode(userModel.getPassword()))
                .goals(List.of())
                .build();

        userRepository.save(user);


        logger.info("User with email {} is registered successfully", user.getEmail());
        return user;
    }

    @Override
    public void saveVerificationToken(User user, String token) {
        VerificationToken verificationToken = new VerificationToken(user.getId(), token);
        verificationTokenRepository.save(verificationToken);
    }

    @Override
    public TokenResult validateVerificationToken(String token) {
        var verificationToken = verificationTokenRepository.findByTokenString(token);
        if(verificationToken.isEmpty()) {
            return TokenResult.INVALID;
        }

        Date currentTime = Calendar.getInstance().getTime();
        Date tokenTime = verificationToken.get().getExpirationTime();

        if(tokenTime.getTime() - currentTime.getTime() <= 0) {
            return TokenResult.EXPIRED;
        }

        Optional<User> user = userRepository.findById(verificationToken.get().getUserId());

        if(user.isPresent()) {
            user.get().setEnabled(true);
            userRepository.save(user.get());
        }

        return TokenResult.VALID;
    }

    public User getUserByEmail(String email) throws UserNotFoundException {
        var user = userRepository.findUserByEmail(email);
        if(user.isEmpty())
            throw new UserNotFoundException("User with this email is not found");

        return user.get();
    }

    @Override
    public void savePasswordResetToken(User user, String token) {
        PasswordToken passwordToken = new PasswordToken(user.getId(), token);
        passwordTokenRepository.save(passwordToken);
    }

    @Override
    public TokenResult validatePasswordResetToken(String token) {
        var passwordToken = passwordTokenRepository.findByTokenString(token);

        if(passwordToken.isEmpty()) {
            return TokenResult.INVALID;
        }

        Date currentTime = Calendar.getInstance().getTime();
        Date tokenTime = passwordToken.get().getExpirationTime();

        if(tokenTime.getTime() - currentTime.getTime() <= 0) {
            return TokenResult.EXPIRED;
        }

        return TokenResult.VALID;
    }

    @Override
    public PasswordToken getPasswordTokenByToken(String token) throws TokenInvalidException {
        Optional<PasswordToken> passwordToken = passwordTokenRepository.findByTokenString(token);
        if(passwordToken.isEmpty())
            throw new TokenInvalidException("Password token is invalid");

        return passwordToken.get();
    }

    @Override
    public User getUserById(String userId) throws UserNotFoundException {
        var user = userRepository.findById(userId);
        if(user.isEmpty())
            throw new UserNotFoundException("User with this id is not found");
        return user.get();
    }

    @Override
    public void savePassword(User user, String newPassword) {
        user.setPassword(newPassword);
        userRepository.save(user);
    }

    @Override
    public boolean checkIfValidPassword(User user, String password) {
        return passwordEncoder.matches(password, user.getPassword());
    }

    @Override
    public void saveUserConfig(String userId, ConfigModel config) throws UserNotFoundException {
        var userOpt = userRepository.findById(userId);
        if(userOpt.isEmpty())
            throw new UserNotFoundException("User with this id is not found");

        User user = userOpt.get();
        UserConfig userConfig = UserConfig.builder()
                .workInterval(config.getWorkInterval())
                .restInterval(config.getRestInterval())
                .workIntervalHeavy(config.getWorkIntervalHeavy())
                .workIntervalLight(config.getWorkIntervalLight())
                .workIntervalMixed(config.getWorkIntervalMixed())
                .restIntervalHeavy(config.getRestIntervalHeavy())
                .restIntervalLight(config.getRestIntervalLight())
                .restIntervalMixed(config.getRestIntervalMixed())
                .build();

        user.setUserConfig(userConfig);
        userRepository.save(user);

        logger.info("Config is saved successfully for user with id: {}", userId);

    }

    @Override
    public UserConfig getUserConfig(String userId) throws UserNotFoundException {
        var userOpt = userRepository.findById(userId);
        if(userOpt.isEmpty())
            throw new UserNotFoundException("User with this id is not found");

        return userOpt.get().getUserConfig();
    }

    @Override
    public void saveUserGoal(String id, Goal goal) throws UserNotFoundException {
        var userOpt = userRepository.findById(id);
        if(userOpt.isEmpty())
            throw new UserNotFoundException("User with this id is not found");

        userOpt.get().getGoals().add(goal);

        this.userRepository.save(userOpt.get());

        logger.info("User goal saved successfully for user with id: {}", id);
    }

    @Override
    public List<Goal> getUserGoals(String id) throws UserNotFoundException {
        var userOpt = userRepository.findById(id);
        if(userOpt.isEmpty())
            throw new UserNotFoundException("User with this id is not found");

        return userOpt.get().getGoals();
    }

    @Override
    public void removeUserGoal(String userId, String goalId) throws UserNotFoundException {
        var userOpt = userRepository.findById(userId);
        if(userOpt.isEmpty())
            throw new UserNotFoundException("User with this id is not found");

        userOpt
                .get()
                .setGoals(
                        userOpt.get().getGoals().stream().filter(e -> !e.getGoalId().equals(goalId)).toList()
                );

        this.userRepository.save(userOpt.get());
        logger.info("User goal with id {} successfully deleted for user with id: {}", goalId, userId);
    }

    @Override
    public VerificationToken generateNewVerificationToken(String oldToken) throws TokenInvalidException {
        Optional<VerificationToken> verificationToken = verificationTokenRepository.findByTokenString(oldToken);
        if(verificationToken.isEmpty()) {
            throw new TokenInvalidException("Verification token is invalid");
        }

        verificationToken.get().setTokenString(UUID.randomUUID().toString());
        verificationToken.get().refreshExpirationTime();
        verificationTokenRepository.save(verificationToken.get());

        return verificationToken.get();
    }
}
