package com.example.userserviceclient.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ConfigModel {
    private Integer restInterval;
    private Integer restIntervalHeavy;
    private Integer restIntervalLight;
    private Integer restIntervalMixed;
    private Integer workInterval;
    private Integer workIntervalHeavy;
    private Integer workIntervalLight;
    private Integer workIntervalMixed;
}
