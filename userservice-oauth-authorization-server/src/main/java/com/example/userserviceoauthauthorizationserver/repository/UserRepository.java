package com.example.userserviceoauthauthorizationserver.repository;

import com.example.userserviceoauthauthorizationserver.collection.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

    Optional<User> findUserByEmail(String email);
}
