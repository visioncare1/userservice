package com.example.userserviceoauthauthorizationserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserserviceOauthAuthorizationServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserserviceOauthAuthorizationServerApplication.class, args);
	}

}
